package org.myorg.tasks.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class MultiField {

	@Inject
	@Named("dob")
	@Optional
	private String dob;

	@Inject
	@Named("firstname")
	@Optional
	private String firstname;

	@Inject
	@Named("lastname")
	@Optional
	private String lastname;

	@Inject
	@Named("email")
	@Optional
	private String email;

	public String getDob() {
		return dob;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	@PostConstruct
	protected void init() {

	}

}
