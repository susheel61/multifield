package org.myorg.tasks.core.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.myorg.tasks.core.models.MultiField;

import com.adobe.cq.sightly.WCMUsePojo;

public class MultiFieldUse extends WCMUsePojo {

	private List<MultiField> multifieldItems;

	public List<MultiField> getMultiFieldItems() {
		Resource childResource = getResource().getChild("items");

		if (childResource!=null) {
			multifieldItems = new ArrayList<MultiField>();
			Iterator<Resource> mf = childResource.listChildren();
			while (mf.hasNext()) {
				MultiField mfitems = mf.next().adaptTo(MultiField.class);
				if (mfitems != null) {
					multifieldItems.add(mfitems);
				}
			}
		}
		return multifieldItems;
	}

	@Override
	public void activate() throws Exception {
		//empty
	}

}
